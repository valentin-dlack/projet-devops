FROM php:apache
WORKDIR /var/www
COPY src/ /var/www/html/
EXPOSE 80
RUN apt-get update && apt-get install -y
RUN docker-php-ext-install mysqli pdo pdo_mysql
CMD ["apache2-foreground"]